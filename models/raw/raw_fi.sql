{{
    config(
        materialized='incremental'
    )
}}




select *
from {{ source('viki', 'Fire_Incidents') }}
{% if is_incremental() %}
    where IncidentDate > (select max(IncidentDate) from {{ this }})
   -- delete from {{ this }} where IncidentDate::date = 
    --(select max(IncidentDate)::date from {{ source('viki', 'Fire_Incidents') }});
    -- this filter will only be applied on an incremental run  
{% endif %}