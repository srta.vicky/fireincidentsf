{{
    config(
        materialized='incremental'
    )
}}
   
with source_fi as(


    select IncidentDate, IncidentNumber,
        AlarmDtTm, 
        ArrivalDtTm, 
        CloseDtTm, 
        cast(EXTRACT(EPOCH FROM age(arrivaldttm, alarmdttm)::INTERVAL)/60 as INTEGER) d_llamada_llegada_min,
        cast(EXTRACT(EPOCH FROM age(closedttm, alarmdttm)::INTERVAL)/60 as INTEGER) duracion_incidente_min,
        Battalion, 
        SuppressionUnits, 
        SuppressionPersonnel, 
        EMSPersonnel, 
        OtherUnits, 
        OtherPersonnel, 
        EstimatedPropertyLoss, 
        EstimatedContentsLoss, 
        FireFatalities, 
        FireInjuries, 
        CivilianFatalities, 
        CivilianInjuries, 
        Numberoffloorswithminimumdamage, 
        Numberoffloorswithsignificantdamage, 
        Numberoffloorswithheavydamage, 
        Numberoffloorswithextremedamage, 
        DetectorsPresent, 
        neighborhood_district,
        false as to_delete
    from {{ source('viki', 'Fire_Incidents') }}
    {% if is_incremental() %}
        where IncidentDate > (select max(IncidentDate) from {{ this }})
        --delete from {{ this }} 
        --where IncidentDate::date = (select max(IncidentDate)::date 
        --from {{ source('viki', 'Fire_Incidents') }});
    -- this filter will only be applied on an incremental run  
    {% endif %}
    


)
,

final as(
    
     select 
        IncidentDate, neighborhood_district, Battalion, count(*) cant_fi, 
        max(IncidentNumber) max_IncidentNumber_for_group,
        min(d_llamada_llegada_min) d_llamada_llegada_min_minimo,
        max(d_llamada_llegada_min) d_llamada_llegada_min_maximo,
        sum(d_llamada_llegada_min) d_llamada_llegada_min_sum,
        min(duracion_incidente_min) duracion_incidente_min_min,
        max(duracion_incidente_min) duracion_incidente_min_max,
        sum(duracion_incidente_min) duracion_incidente_min_sum,
        sum(SuppressionUnits) SuppressionUnits, 
        sum(SuppressionPersonnel) SuppressionPersonnel, 
        sum(EMSPersonnel) EMSPersonnel, 
        sum(OtherUnits) OtherUnits, 
        sum(OtherPersonnel) OtherPersonnel, 
        sum(EstimatedPropertyLoss) EstimatedPropertyLoss, 
        sum(EstimatedContentsLoss) EstimatedContentsLoss,  
        sum(FireFatalities) FireFatalities, 
        sum(FireInjuries) FireInjuries, 
        sum(CivilianFatalities) CivilianFatalities, 
        sum(CivilianInjuries) CivilianInjuries, 
        sum(Numberoffloorswithminimumdamage) Numberoffloorswithminimumdamage,
        sum(Numberoffloorswithsignificantdamage) Numberoffloorswithsignificantdamage, 
        sum(Numberoffloorswithheavydamage) Numberoffloorswithheavydamage, 
        sum(Numberoffloorswithextremedamage) Numberoffloorswithextremedamage
    from source_fi
    group by IncidentDate, neighborhood_district, Battalion
    
)
select * from final



